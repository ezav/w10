$(document).on("ready", function() { //при загрузке документа
  $(".regular").slick({
    
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
	
    responsive: [
        {
          breakpoint: 1024,  //ограничение ширины экрана
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true
          }
        }
    ]
  });


  $(".ajaxForm").submit(function(e){
      e.preventDefault(); //Если будет вызван данный метод, то действие события по умолчанию не будет выполнено
      var href = $(this).attr("action");
	  
      $.ajax({
        type: "POST", 
 		dataType: "json",
		url: href,
        data: $(this).serialize(), // .serialize() преобразование щначения элементов в строку данных
        success:
		function(response){
        if(response.status == "success"){
          alert("ПРИНЯТО!");
        }else {
          alert("ОШИБКА: " + response.message);
        }
      }
    });
  });

});




function clickButtom(){
  document.getElementById("InputName1").value=""; 
  document.getElementById("InputPhoneNumber1").value="";
  selectElement('region', 'default')
  document.getElementById("textareaNoResize").value="";
}

function fioInput(e){
  console.log(e.target.value)   //выводит отладочную информацию в консоль, т.е. скрывая ее от пользователей.
  localStorage.setItem("FIO", e.target.value)
}

function phoneInput(e){
  console.log(e.target.value)
  localStorage.setItem("number", e.target.value) //ключ+значение запистаь в хранилище
}

function regionInput(e){
  console.log(e.target.value)
  localStorage.setItem("region", e.target.value)
}

function messageInput(e){
  console.log(e.target.value)
  localStorage.setItem("message", e.target.value)
}

///////////////
function setFio(){
  if (localStorage.getItem("FIO") !== null){
	  //вернуть ссылку элемента по идентефикатору
    document.getElementById("InputName1").value=localStorage.getItem("FIO")
  }
}

function setPhone(){
  if (localStorage.getItem("number") !== null){
    document.getElementById("InputPhoneNumber1").value =localStorage.getItem("number")
  }
}

function setRegion(){
  if (localStorage.getItem("region") !== null){
    document.getElementById("region").value=localStorage.getItem("region")
  }
}

function setMessage(){
  if (localStorage.getItem("message") !== null){
    document.getElementById("textareaNoResize").value=localStorage.getItem("message")
  }
}


function selectElement(id, valueToSelect) {    
    let element = document.getElementById(id);
    element.value = valueToSelect;
}

setFio();
setPhone();
setRegion();
setMessage();




function myButton(){
  history.pushState({page: 2}, "title 2", "?page=2"); //добавить новое состояние в браузер
  exampleModalCenter.style.display = "flex";
  }
  

  window.onclick = function(event) {
  if (event.target == exampleModalCenter) {
    $('#exampleModalCenter').hide();
    }
  }
  
  addEventListener("popstate",function(){ //при изменении активерй записи истории
      history.pushState({page: 1}, "title 1", "?page=1");
	  
      $('#exampleModalCenter').hide();
      $("body").attr("style", "overflow: unset"); //стиль, полосы прокрутки и т.д наследуется от родительскоро класса
  }, true);


  function zakr(){
    $('exampleModalCenter').hide();
  }
 